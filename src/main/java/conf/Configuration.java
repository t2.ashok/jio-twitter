package conf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.IOUtils;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

public enum Configuration {
    CONFIGURATION;
    private String CONFIG_FILE_NAME = "app.properties";
    private Logger logger = LoggerFactory.getLogger(Configuration.class);
    private Properties props = null;

    Configuration() {
        FileReader reader = null;
        try {
            File file = new File(getClass().getClassLoader().getResource(CONFIG_FILE_NAME).getFile());
            reader = new FileReader(file);
            props = new Properties();
            props.load(reader);
        } catch (Exception ex) {
            logger.error("Error loading bank loan rates ", ex);
        } finally {
            IOUtils.close(reader);
        }
    }

    public String getValue(String key) {
        return props.getProperty(key);
    }
}
