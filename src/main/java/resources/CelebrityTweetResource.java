package resources;

import conf.Configuration;
import core.CelebrityProcessor;
import core.CelebrityProcessorImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.JsonUtil;

import java.util.ArrayList;
import java.util.List;

import static spark.Spark.get;

public class CelebrityTweetResource {

    private static final Logger logger = LoggerFactory.getLogger(CelebrityTweetResource.class);
    private static CelebrityProcessor celebrityProcessor;

    public static void main(String[] args) {

        celebrityProcessor = new CelebrityProcessorImpl();
        Configuration conf = Configuration.CONFIGURATION;
        final List<String> celebrities = new ArrayList<>();


        get("/indian-celebrity/tweets", (request, response) -> {
            int size = Integer.parseInt(request.queryParams("size"));
            return JsonUtil.toJson(celebrityProcessor.getCelebrityTweets(size));
        });
    }

}
