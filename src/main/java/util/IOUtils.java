package util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;

public class IOUtils {
    private static final Logger logger = LoggerFactory.getLogger(IOUtils.class);

    public static void close(Object... resources) {
        try {
            for (Object resource : resources) {
                if (resource instanceof Reader) {
                    Reader reader = (Reader) resource;
                    reader.close();
                }
            }
        } catch (IOException ex) {
            logger.error("Error in closing resources ", ex);
        }
    }
}
