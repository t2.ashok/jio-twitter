package util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class JsonUtil {
    private static final Logger logger = LoggerFactory.getLogger(JsonUtil.class);
    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static <K> Object fromJson(String json, Class<K> klass) {
        K k = null;
        try {
            k = (K) MAPPER.readValue(json, klass);
        } catch (IOException e) {
            logger.error("Error in reading values from json", e);
        }
        return k;
    }

    public static String toJson(Object o) {
        String s = null;
        try {
            s = MAPPER.writeValueAsString(o);
        } catch (JsonProcessingException e) {

            logger.error("Error in converting to json ", e);
        }
        return s;
    }
}
