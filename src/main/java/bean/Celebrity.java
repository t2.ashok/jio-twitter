package bean;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"message"})
public class Celebrity {

    private String name;
    private String message;


}
