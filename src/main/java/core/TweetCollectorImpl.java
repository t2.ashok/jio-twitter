package core;

import bean.Celebrity;
import conf.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

//rohit.jha@ril.com
public class TweetCollectorImpl implements TweetCollector {

    private static final Logger logger = LoggerFactory.getLogger(TweetCollectorImpl.class);

    @Override
    public Set<Celebrity> getMessage(int size) {
        Twitter twitter = new TwitterFactory(createNewBuilder().build()).getInstance();
        Set<Celebrity> celebrityList = new HashSet<>();
        int pageno = 1;
        while (true) {
            try {

                Paging page = new Paging(pageno++, 100);
                ResponseList<Status> userTimeline = null;
                QueryResult result = twitter.search(new Query());
                List<Status> tweets = result.getTweets();
                tweets.forEach(t -> {
                    logger.info("messages " + t.getText());
                    if (t.getPlace() != null && t.getPlace().getCountry().equalsIgnoreCase("India") &&
                            t.getUser().getFollowersCount() > 10000) {

                        Celebrity celebrity = new Celebrity();
                        celebrity.setName(t.getUser().getName());
                        celebrity.setMessage(t.getText());
                        celebrityList.add(celebrity);
                    }
                });

            } catch (TwitterException e) {
                logger.error("Error in getting twitter messages", e);
            }
            if (celebrityList.size() > size) {
                break;
            }
        }
        return celebrityList;

    }


    public ConfigurationBuilder createNewBuilder() {
        Configuration conf = Configuration.CONFIGURATION;
        ConfigurationBuilder cb = new ConfigurationBuilder();
        /*cb.setOAuthConsumerKey("pWfYAui8tyYa2ESyeCYRx9oI8");
        cb.setOAuthConsumerSecret("8kbjSFg8GNr17qhgVI45tSdCLeLISZ0MxXJGgVE3leVIHzkcs5");
        cb.setOAuthAccessToken("904953399375937536-XKRDhSVgDREHTBJzfmz3WbrR4RRBS0b");
        cb.setOAuthAccessTokenSecret("Za4intqRTZobnwR6tDAbWG27jgFBQuZXle0M4HvMl7Ogl");*/

        cb.setOAuthConsumerKey(conf.getValue("consumerKey"));
        cb.setOAuthConsumerSecret(conf.getValue("consumerSecret"));
        cb.setOAuthAccessToken(conf.getValue("oauthKey"));
        cb.setOAuthAccessTokenSecret(conf.getValue("oauthSecret"));

        return cb;

    }
}
