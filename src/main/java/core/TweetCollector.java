package core;

import bean.Celebrity;

import java.util.List;
import java.util.Set;

public interface TweetCollector {

    Set<Celebrity> getMessage(int size);

}
