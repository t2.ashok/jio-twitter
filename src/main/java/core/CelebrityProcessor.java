package core;

import bean.Celebrity;

import java.util.List;
import java.util.Set;

public interface CelebrityProcessor {

    public Set<Celebrity> getCelebrityTweets(int size);
}
