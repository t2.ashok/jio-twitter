package core;

import bean.Celebrity;

import java.util.List;
import java.util.Set;

public class CelebrityProcessorImpl implements CelebrityProcessor{


    private TweetCollector tweetCollector;
    public CelebrityProcessorImpl(){
        tweetCollector = new TweetCollectorImpl();
    }
    @Override
    public Set<Celebrity> getCelebrityTweets(int size) {
        return tweetCollector.getMessage(size);
    }
}
